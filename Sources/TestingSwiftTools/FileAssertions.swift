import Foundation
import XCTest

/// Asserts that there is a file at the given url.
/// - Parameters:
///   - url: The URL of the file to check for the existence of.
///   - message: An optional description of a failure.
///   - file: The file where the failure occurs.
///     The default is the filename of the test case where you call this function.
///   - line: The line number where the failure occurs.
///     The default is the line number where you call this function.
public func TSAssertURLExists(
    _ url: URL,
    _ message: @autoclosure () -> String = "",
    file: StaticString = #filePath,
    line: UInt = #line
) {
    guard url.exists else {
        let customMessage = message()
        if customMessage.isEmpty {
            XCTFail("URL(\"\(url.absoluteString)\") does not exist.", file: file, line: line)
        } else {
            XCTFail(customMessage, file: file, line: line)
        }
        return
    }
}

/// Asserts that there is a file at the given url.
/// - Parameters:
///   - url: The URL of the file to check for the existence of.
///   - message: An optional description of a failure.
///   - file: The file where the failure occurs.
///     The default is the filename of the test case where you call this function.
///   - line: The line number where the failure occurs.
///     The default is the line number where you call this function.
public func TSAssertURLNotExists(
    _ url: URL,
    _ message: @autoclosure () -> String = "",
    file: StaticString = #filePath,
    line: UInt = #line
) {
    guard !url.exists else {
        let customMessage = message()
        if customMessage.isEmpty {
            XCTFail("Expected URL(\"\(url.absoluteString)\") to not exist.", file: file, line: line)
        } else {
            XCTFail(customMessage, file: file, line: line)
        }
        return
    }
}
