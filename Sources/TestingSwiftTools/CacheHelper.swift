import Foundation

/// Helper methods for working with the cache directory.
public enum CacheHelper {
    /// Generate a new, empty, directory in the cache directory.
    ///
    /// The directory will have the first available positive integer as its name.
    /// (Starting with “1”, if that directory doesn’t exist yet.)
    /// - Parameter subdirPath: Optional path for sub-directory(ies), in the cache directory, to put the new directory in.
    /// - Returns: The `URL` for the new directory.
    /// - Throws: If `FileManager` can’t find/access the cache directory,
    ///   or can’t create the new directory.
    public static func newCleanCacheDirectory(in subdirPath: String = "") throws -> URL {
        let cacheURL = try FileManager.default.url(
            for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true
        )
        let parentURL = subdirPath.isEmpty ? cacheURL : cacheURL.appendingPathComponent(subdirPath)
        var nextCacheDir: Int = 1
        var newURL = parentURL.appendingPathComponent("\(nextCacheDir)")
        while newURL.exists {
            nextCacheDir += 1
            newURL = parentURL.appendingPathComponent("\(nextCacheDir)")
        }
        try FileManager.default.createDirectory(at: newURL, withIntermediateDirectories: true)
        return newURL
    }
}
