import Foundation

extension URL {
    /// Get an url for the caches directory.
    /// - Returns: The `URL` of the caches directory.
    public static var cache: URL? {
        let cacheURLs = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)
        return cacheURLs.first
    }

    /// Get an url for the user documents directory.
    /// - Returns: The `URL` of the user documents directory.
    public static var documents: URL? {
        let dirURLs = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return dirURLs.first
    }

    /// Determine if the url points to a resource (file/directory) that exists.
    /// - Returns: `true` if there is a resource at the url.
    public var exists: Bool {
        FileManager.default.fileExists(atPath: self.relativePath)
    }

    /// Get the number of resources (files and directories) in a directory.
    ///
    /// The URL must be for a directory on the local filesystem.
    /// - Returns: The number of resources in the directory,
    ///   or nil if the URL could not be accessed as a local directory.
    public var resourceCount: Int? {
        let subpaths = try? FileManager.default.contentsOfDirectory(atPath: self.relativePath)
        return subpaths?.count
    }

    /// Removes the file or directory at the specified URL.
    /// - Throws: If `FileManager` is unable to remove the specified resource.
    public func remove() throws {
        try FileManager.default.removeItem(at: self)
    }

    /// If there is a file at the given url, delete it.
    /// - Parameter url: The url of the file to remove, if it exists.
    /// - Throws: If `FileManager` is unable to remove the specified resource.
    public func ensureDoesNotExist() throws {
        if self.exists {
            try self.remove()
        }
    }

    /// Delete any files, with the given file extension, in the local filesystem directory specified by the URL.
    ///
    /// *Important:* The URL must be for a local directory.
    /// - Parameter extension: The file extension (without leading `.`) specifying which type of files to remove.
    /// - Throws: FileManager errors if the directory cannot be accessed, or a file failed to be removed.
    public func removeFiles(extension fileExtension: String) throws {
        let files = try FileManager.default.contentsOfDirectory(
            at: self, includingPropertiesForKeys: nil
        )
        let matchingFiles = files.filter { fileURL in
            fileURL.lastPathComponent.hasSuffix(".\(fileExtension)")
        }
        for url in matchingFiles {
            if url.isFileURL && url.pathExtension == fileExtension {
                try FileManager.default.removeItem(at: url)
            }
        }
    }
}
