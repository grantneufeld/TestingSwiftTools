# Testing Swift Tools

A Swift Package collecting various functions to help with testing.

---

## Usage

### URL Extensions

These are computed properties and functions that are added to the `URL` class.

#### Accessing Directories

##### `.cache`

This returns a `URL` for the Caches directory (or `nil` if unavailable).

```swift
let cachesDirectoryURL = URL.cache
```

##### `.documents`

This returns a `URL` for the Documents directory (or `nil` if unavailable).

```swift
let documentsDirectoryURL = URL.documents
```

##### `.resourceCount`

This returns the number of resources (files and directories) in the local filesystem directory pointed to by the `URL`, or `nil` if the url cannot be accessed as a directory.

```swift
let filesInDirectory = directoryURL.resourceCount
```

#### Resource Existence

##### `.exists`

Determine if the url points to a resource (file/directory) that exists on the local filesystem.

```swift
let resourceExists = myURL.exists
```

#### Deleting Files & Directories

##### `remove()`

Tries to remove the file or directory.

```swift
try myURL.remove()
```

##### `ensureDoesNotExist()`

Almost identical to `remove()`, except this one will check if the file exists first, so it won’t throw an error if there is no file. (But, it will still throw other errors, if encountered.)

```swift
try myURL.ensureDoesNotExist()
```

##### `removeFiles(extension:)`

Delete any files, with the given file extension, in the local filesystem directory specified by the URL.

```swift
try myURL.removeFiles(extension: "txt")
```

### Cache Helper

#### Creating Directories in the Caches Directory

##### `newCleanCacheDirectory(in:)`

Generate a new, empty, directory in the cache directory.

The directory will have the first available positive integer as its name.
(Starting with “1”, if that directory doesn’t exist yet.)

The `in:` parameter is optional, and can specify a path to a subdirectory. E.g., `in: "abc/def"` would result in a path to a subdirectory like `…/Caches/abc/def/`

```swift
let newCacheDirURL = try CacheHelper.newCleanCacheDirectory(in: "my/subdir")
```

### File Assertions

#### Does the Resource (File/Directory) Exist?

##### `TSAssertURLExists()`

Asserts that the given URL points to a resource (file or directory) that exists on the local filesystem.

```swift
TSAssertURLExists(myURL)
```

##### `TSAssertURLNotExists()`

The inverse of `TSAssertURLExists()`. Asserts that the given URL does not exist on the local filesystem.

```swift
TSAssertURLNotExists(myURL)
```

### Async Assertions

#### Testing Asynchronous Functions

##### `TSAssertThrowsAsyncError()`

Asserts that an asynchronous expression throws an error.
(Intended to function as a drop-in asynchronous version of `XCTAssertThrowsError`.)

```swift
await TSAssertThrowsAsyncError(
    try await sut.function()
) { error in
    XCTAssertEqual(error as? MyError, MyError.specificError)
}
```

---

## Legal

### Copyright

Copyright ©2024 [Grant Neufeld](https://gitlab.com/grantneufeld).

### License

All Rights Reserved.
