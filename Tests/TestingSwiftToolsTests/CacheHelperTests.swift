import Foundation
@testable import TestingSwiftTools
import XCTest

final class CacheHelperTests: XCTestCase {
    // MARK: - newCleanCacheDirectory()

    func test_newCleanCacheDirectory_givenCache1DoesNotExist_createsDirAndReturnsURL() throws {
        let expectedURL = self.cacheURL.appendingPathComponent("1")
        TSAssertURLNotExists(expectedURL)
        let resultURL = try CacheHelper.newCleanCacheDirectory()
        XCTAssertEqual(resultURL, expectedURL)
        TSAssertURLExists(expectedURL)
        // cleanup
        try resultURL.ensureDoesNotExist()
    }

    func test_newCleanCacheDirectory_givenCache1Exists_createsDirAndReturnsURL() throws {
        let cache1URL = self.ensureCacheDirExists(1)
        defer {
            try? cache1URL.ensureDoesNotExist()
        }
        let expectedURL = self.cacheURL.appendingPathComponent("2")
        TSAssertURLNotExists(expectedURL)
        let resultURL = try CacheHelper.newCleanCacheDirectory()
        XCTAssertEqual(resultURL, expectedURL)
        TSAssertURLExists(expectedURL)
        // cleanup
        try resultURL.ensureDoesNotExist()
    }

    func test_newCleanCacheDirectory_whenSubdirSpecified_createsDirAndReturnsURL() throws {
        let expectedURL = self.cacheURL.appendingPathComponent("_test/TS/1")
        TSAssertURLNotExists(expectedURL)
        let resultURL = try CacheHelper.newCleanCacheDirectory(in: "_test/TS")
        XCTAssertEqual(resultURL, expectedURL)
        TSAssertURLExists(expectedURL)
        // cleanup
        try resultURL.ensureDoesNotExist()
    }

    // MARK: - Test Helpers

    private var cacheURL: URL {
        try! FileManager.default.url(
            for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true
        )
    }

    private func ensureCacheDirExists(_ index: Int = 1) -> URL {
        let newURL = cacheURL.appendingPathComponent("\(index)")
        guard !newURL.exists else {
            return newURL
        }
        try! FileManager.default.createDirectory(at: newURL, withIntermediateDirectories: false)
        return newURL
    }
}
