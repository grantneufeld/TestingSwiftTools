@testable import TestingSwiftTools
import XCTest

final class URLHelpersTests: XCTestCase {
    func test_cache_returnsTheCacheURL() throws {
        let resultURL = URL.cache
        let expectedURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first
        XCTAssertEqual(resultURL, expectedURL)
    }

    func test_documents_returnsTheDocumentsURL() throws {
        let resultURL = URL.documents
        let expectedURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
        XCTAssertEqual(resultURL, expectedURL)
    }

    // MARK: - .exists

    func test_exists_givenNonExistentResource_returnsFalse() throws {
        let sut = URL(filePath: "/this_does_not_exist")
        XCTAssertFalse(sut.exists)
    }

    func test_exists_givenResourceExists_returnsTrue() throws {
        let sut = URL(filePath: "/bin")
        XCTAssertTrue(sut.exists)
    }

    // MARK: - .resourceCount

    func test_resourceCount_givenEmptyDirectory_returnsZero() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        defer {
            try? sut.ensureDoesNotExist()
        }
        XCTAssertEqual(sut.resourceCount, 0)
    }

    func test_resourceCount_givenOneFileInDirectory_returnsOne() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        defer {
            try? sut.ensureDoesNotExist()
        }
        _ = try self.fileFactory(in: sut, fileExt: "one")
        XCTAssertEqual(sut.resourceCount, 1)
    }

    func test_resourceCount_givenNonExistentURL_returnsNil() throws {
        let sut = URL(filePath: "/non/existent")
        XCTAssertNil(sut.resourceCount)
    }

    // MARK: - remove()

    func test_remove_givenNonExistentResource_throwsError() throws {
        let sut = URL(filePath: "/this_does_not_exist")
        XCTAssertThrowsError(try sut.remove()) { error in
            let nsError = error as NSError
            XCTAssertEqual(nsError.domain, "NSCocoaErrorDomain")
            XCTAssertEqual(nsError.code, 4)
        }
    }

    func test_remove_givenResourceExists_removesTheResource() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        TSAssertURLExists(sut)
        try sut.remove()
        TSAssertURLNotExists(sut)
    }

    // MARK: - ensureDoesNotExist()

    func test_ensureDoesNotExist_givenNonExistentResource_doesNothing() throws {
        let sut = URL(filePath: "/this/does/not/exist")
        XCTAssertNoThrow(try sut.ensureDoesNotExist())
    }

    func test_ensureDoesNotExist_givenResourceExists_removesTheResource() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        TSAssertURLExists(sut)
        try sut.ensureDoesNotExist()
        TSAssertURLNotExists(sut)
    }

    // MARK: - removeFiles()

    func test_removeFiles_givenDirectoryDoesNotExist_throwsError() throws {
        let sut = URL(filePath: "/nope")
        XCTAssertThrowsError(try sut.removeFiles(extension: "notdir")) { error in
            let nsError = error as NSError
            XCTAssertEqual(nsError.domain, "NSCocoaErrorDomain")
            XCTAssertEqual(nsError.code, 260)
        }
    }

    // given directory cannot be read

    func test_removeFiles_givenDirectoryIsEmpty_doesNothing() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        defer {
            try? sut.ensureDoesNotExist()
        }
        XCTAssertNoThrow(try sut.removeFiles(extension: "txt"))
    }

    func test_removeFiles_givenDirectoryHasNonMatchingFiles_doesNothing() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        defer {
            try? sut.ensureDoesNotExist()
        }
        _ = try self.fileFactory(in: sut, fileExt: "txt")
        // make sure the expected files exist
        XCTAssertEqual(sut.resourceCount, 1)
        XCTAssertNoThrow(try sut.removeFiles(extension: "none"))
    }

    func test_removeFiles_givenDirectoryHasOneMatchingFile_removesTheFile() throws {
        let sut = try CacheHelper.newCleanCacheDirectory(in: Self.testCachePath)
        defer {
            try? sut.ensureDoesNotExist()
        }
        let files = try self.fileFactory(in: sut, fileExt: "txt")
        // make sure the expected files exist
        XCTAssertEqual(sut.resourceCount, 1)
        try sut.removeFiles(extension: "txt")
        let theFile = try XCTUnwrap(files.first)
        XCTAssertFalse(theFile.exists)
    }

    // MARK: - Test Helpers

    private static let testCachePath = "_test/TS/URL"

    private func fileFactory(in url: URL, fileExt: String, count: Int = 1) throws -> [URL] {
        var result: [URL] = []
        for index in 1...count {
            let newURL = url.appendingPathComponent("factory_\(count).\(fileExt)")
            try "\(index)".write(to: newURL, atomically: true, encoding: .utf8)
            result.append(newURL)
        }
        return result
    }

    private func cleanup(_ urls: [URL]) {
        for url in urls {
            try? url.ensureDoesNotExist()
        }
    }
}
